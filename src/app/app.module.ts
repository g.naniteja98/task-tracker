import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskApiService } from './API_LAYER/task-api.service';
import {HttpClientModule} from '@angular/common/http';
import { NewTaskComponent } from './PAGES/new-task/new-task.component';
import { TaskListComponent } from './PAGES/task-list/task-list.component';
import { DashboardComponent } from './PAGES/dashboard/dashboard.component';
import { CurrentStatusComponent } from './PAGES/current-status/current-status.component';
import { TaskDetailsComponent } from './PAGES/task-details/task-details.component';
import { ReduceTextPipe } from './custom-pipes/reduce-text.pipe'
@NgModule({
  declarations: [
    AppComponent,
    NewTaskComponent,
    TaskListComponent,
    DashboardComponent,
    CurrentStatusComponent,
    TaskDetailsComponent,
    ReduceTextPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [TaskApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
